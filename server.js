const path       = require('path');

const express    = require('express');
const bodyParser = require('body-parser');
const cors       = require('cors');
const helmet     = require('helmet');
const upload     = require('express-fileupload');
const mongoose   = require('mongoose');

const {chatroom} = require('./socket/chatroom');

//TODO connect server to mongodb 
(async function(){
    try{
        await mongoose.connect('mongodb://localhost:27017/portfolio');
        console.log('mongodb is connected!');
    }catch(err){
        throw err;
    }
})();

//TODO setup app:
const app = express();
const STEP = 'development';

//TODO setup config in development mode:
if(STEP === 'development'){
    const dotenv = require('dotenv');
    dotenv.config({path:'./config/config.env'});
}

//TODO setup modules:
app.use(cors());
app.use(upload());
app.use(helmet());
app.use(bodyParser.urlencoded({extended:false}));

//TODO setup statics:
app.use(express.static(path.join(__dirname,'public')));

//todo-> manage routers:
app.use('/controller',require('./routers/route').router);

app.get('/',async(req,res)=>{
  res.send('hello')
})

//TODO initialize server PORT and real-time server (socket.io):
const server = require('http').Server(app)
const io = require('socket.io')(server, {
    cors:{
      origins:["*"],
      handlePreflightRequest:(req,res)=>{
        res.writeHead(200,{
          "Access-Control-Allow-Origin": "*",
          'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
          "Access-Control-Allow-Headers": "vi_chat",
          'Access-Control-Allow-Credentials': true
        });
        res.end();
      }
    }
});

//TODO setup chatroom server:
chatroom(io);

server.listen(process.env.PORT,()=>{
    console.log(`THE APP IS LISTEN TO PORT ${process.env.PORT}!`);
})