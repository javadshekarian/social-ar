const Joi = require('joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {generate} = require('shortid');
const {jwtDecode} = require('jwt-decode');

const {Chat} = require('../model/Chat');
const {Comment} = require('../model/Comment');
const {convertToError,saveingFile,errors} = require('../helper/helper');
const {userFindOne,userInsertOne, userFindMany} = require('../usage/userUsage');
const {commentInsertOne,commentUpdateOne,commentDeleteOne, commentFindMany} = require('../usage/commentUsage');

module.exports.register = async (req,res) => {
    const {username,password,repeatPassword} = req.body;

    if(username === ''){
        res.status(203).json({error:convertToError(errors.usernameEmpty)});
        return;
    }
    if(password === ''){
        res.status(203).json({error:convertToError(errors.passwordEmpty)});
        return;
    }
    if(password !== repeatPassword){
        res.status(203).json({error:convertToError(errors.invalidPasswords)});
        return;
    }
    if(req.files === null){
        res.status(203).json({error:convertToError(errors.profilePictureEmpty)});
        return;
    }

    const hash = await bcrypt.hash(password,10);
    const userIsExist = await userFindOne({username});
    
    if(userIsExist !== null){
        res.status(203).json({error:convertToError(errors.userIsExistError)});
        return;
    }
    
    const validationSchema = Joi.object().keys({
        username:Joi.string().trim().min(4).max(40).required(),
        password:Joi.string().trim().min(5).required(),
        mimetype:Joi.valid('image/jpg','image/png','image/jpeg','image/webp')
    })

    const isValid = validationSchema.validate({
        username,
        password,
        mimetype:req.files.file.mimetype
    });

    if(isValid.error !== undefined){
        res.status(203).json(isValid);
        return;
    }

    try{
        //! saving profile picture file:
        const fileName = `${generate()}.jpg`;
        await saveingFile(req.files.file,fileName);

        //! save user information:
        const result = await userInsertOne({username,password:hash,pictureName:fileName});

        const token = jwt.sign(
            {username:result.username,pictureName:result.pictureName,_id:result._id},
            process.env.SECRET,
            {expiresIn:'30h'}
        )
    
        res.status(200).json(token);
    }catch(err){
        res.status(203).json({error:convertToError(err)});
    }
}

module.exports.login = async (req,res) => {
    const {username,password} = req.body;
    const result = await userFindOne({username});
    
    if(result === null){
        res.status(203).json({message:errors.userNotExist});
        return;
    }

    const isMatch = await bcrypt.compare(password, result.password);
    
    if(!isMatch){
        res.status(203).json({message:errors.falsePassword});
        return;
    }

    const token = jwt.sign(
        {username:result.username,pictureName:result.pictureName,_id:result._id},
        process.env.SECRET,
        {expiresIn:'30h'}
    )

    res.status(200).json(token);
}

module.exports.addComment = async (req,res) => {
    const {comment,token} = req.body;
    const {_id} = jwtDecode(token);

    const commentValidatioSchema = Joi.object().keys({
        comment:Joi.string().min(10).max(200).required()
    })

    const isValid = commentValidatioSchema.validate({
        comment
    });

    if(isValid.error !== undefined){
        res.status(203).json(isValid);
        return;
    }

    try{
        await commentInsertOne({comment,reference:_id});
        res.status(200).json({message:"comment is saved!"})
    }catch(err){
        res.status(203).json({err});
    }
}

module.exports.checkIsAdmin = async (req,res) => {
    const {username,password} = req.body;
    
    const isMatch = await bcrypt.compare(password,process.env.ADMIN_PASSWORD);
    const usernameIsMatch = await bcrypt.compare(username,process.env.ADMIN_USERNAME);

    if(!isMatch || !usernameIsMatch){
        res.status(203).json({message:errors.falsePassword});
        return;
    }

    const result = await userFindOne({username});
    const token = jwt.sign(
        {username:result.username,pictureName:result.pictureName,_id:result._id},
        process.env.SECRET,
        {expiresIn:'30h'}
    )

    res.status(200).json(token);
}

module.exports.checkAdminInformation = async (req,res) => {
    const token = jwtDecode(req.body.token);
    const {username} = token;

    const usernameIsMatch = await bcrypt.compare(username,process.env.ADMIN_USERNAME);

    if(!usernameIsMatch){
        res.status(203).json({message:'your information is wrong!'});
        return;
    }

    const result = await userFindOne({username});
    const setToken = jwt.sign(
        {username:result.username,pictureName:result.pictureName,_id:result._id},
        process.env.SECRET,
        {expiresIn:'30h'}
    )
    res.status(200).json(setToken);
}

module.exports.allContacts = async (req,res) => {
    try{
        const contacts = await userFindMany();
        res.status(200).json(contacts);
    }catch(err){
        res.status(203).json(err);
    }
}

module.exports.queryMesseges = async (req,res) => {
    try{
        const {from,to} = req.body;
        const result = await Chat.find({$or:[
            {from:from,to:to},
            {from:to,to:from}
        ]})
        .populate({ path: 'reference',select:['pictureName']})
        .sort({'submittedDate': 'desc'})
        .exec();

        res.status(200).json(result);
    }catch(err){
        res.status(500).json({content:"fail"})
    }
}

module.exports.queryComments = async (req,res) => {
    const token = jwtDecode(req.body.token);

    if(token.username !== 'admin'){
        return;
    }

    try{
        const comments = await Comment.find({})
        .populate({path: 'reference',select:['pictureName','username']});
        
        res.status(200).json(comments);
    }catch(err){
        res.status(203).json(err)
    }
}

module.exports.showComment = async (req,res) => {
    try{
        const comment_id = req.body.comment_id;
        const result = await commentUpdateOne({$and:[{_id:comment_id}]},{isShownInPage:true});
        res.status(200).json(result);
    }catch(err){
        res.status(203).json(err);
    }
}

module.exports.notShowComment = async (req,res) => {
    try{
        const comment_id = req.body.comment_id;
        const result = await commentUpdateOne({$and:[{_id:comment_id}]},{isShownInPage:false});
        res.status(200).json(result);
    }catch(err){
        res.status(203).json(err);
    }
}

module.exports.removeComment = async (req,res) => {
    try{
        const comment_id = req.body.comment_id;
        const result = await commentDeleteOne(comment_id);
        res.status(200).json(result);
    }catch(err){
        res.status(203).json(err);
    }
}

module.exports.querySelectedComment = async (req,res) => {
    try{
        const comments = await Comment.find({$and:[
            {isShownInPage:true}
        ]}).populate({
            path:'reference',
            select:['username','pictureName']
        });

        res.status(200).json(comments);
    }catch(err){
        res.status(203).json(err);
    }
}