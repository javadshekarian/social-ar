const {User} = require('../model/User');

module.exports.userFindOne = async (data) => {
    var result = await User.findOne(data);
    return result;
}

module.exports.userFindMany = async (data) => {
    const result = await User.find(data);
    return result;
}

module.exports.userInsertOne = async (data) => {
    const result = new User(data);
    await result.save();
    return result;
}

module.exports.userInsertMany = async (data) => {
    const result = new User(data);
    await result.save();
    return result;
}

module.exports.userUpdateOne = async (filter,update) => {
    const result = await User.findOneAndUpdate(filter,update);
    return result;
}

module.exports.userDeleteOne = async (id) => {
    const result = await User.findByIdAndDelete(id);
    return result;
}