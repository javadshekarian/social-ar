const {Chat} = require('../model/Chat');

module.exports.chatFindOne = async (data) => {
    const result = await Chat.findOne(data);
    return result;
}

module.exports.chatFindMany = async (data) => {
    const result = await Chat.find(data);
    return result;
}

module.exports.chatInsertOne = async (data) => {
    const result = new Chat(data);
    await result.save();
    return result;
}

module.exports.chatInsertMany = async (data) => {
    const result = new Chat(data);
    await result.save();
    return result;
}

module.exports.chatUpdateOne = async (filter,data) => {
    const result = await Chat.findOneAndUpdate(filter,data);
    return result;
}

module.exports.chatDeleteOne = async (id) => {
    const result = await Chat.findByIdAndDelete(id);
    return result;
}