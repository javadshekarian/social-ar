const {Comment} = require('../model/Comment');

module.exports.commentFindOne = async (data) => {
    const result = await Comment.findOne(data);
    return result;
}

module.exports.commentFindMany = async (data) => {
    const result = await Comment.find(data);
    return result;
}

module.exports.commentInsertOne = async (data) => {
    const result = new Comment(data);
    await result.save();
    return result;
}

module.exports.commentInsertMany = async (data) => {
    const result = new Comment(data);
    await result.save();
    return result;
}

module.exports.commentUpdateOne = async (filter,data) => {
    const result = await Comment.findOneAndUpdate(filter,data);
    return result;
}

module.exports.commentDeleteOne = async (id) => {
    const result = await Comment.findByIdAndDelete(id);
    return result;
}