module.exports.convertToError = (errorMessage) => {
    const error = new Object();
    error['details'] = [{message:errorMessage}];
    return error;
}

module.exports.saveingFile = (file,fileName) => {
    return new Promise((resolve,reject)=>{
        file.mv(`./web/public/uploads/${fileName}`,err=>{
            if(err){
                reject(err)
            };
            resolve();
        })
    })
}

module.exports.errors = {
    usernameEmpty:'username cannot be empty. Please choose a username!',
    passwordEmpty:'password cannot be empty. Please choose a password!',
    invalidPasswords:'The passwords you entered are different. Please check the password again',
    profilePictureEmpty:"Profile picture cannot be empty, please choose a picture",
    userIsExistError:"You have account in this website! please click login",
    userNotExist:"user not exist! please click on register",
    falsePassword:"your password or username is false, please inter correct password!"
}