const {Router} = require('express');
const router = new Router();

const controller = require('../controller/controller');

router.post('/register',controller.register);
router.post('/login',controller.login);
router.post('/addComment',controller.addComment);
router.post('/check-is-admin',controller.checkIsAdmin);
router.post('/check-admin-information',controller.checkAdminInformation);
router.post('/all-contacts',controller.allContacts);
router.post('/query-messages',controller.queryMesseges);
router.post('/query-comments',controller.queryComments);
router.post('/show-comment',controller.showComment);
router.post('/notshow-comment',controller.notShowComment);
router.post('/remove-comment',controller.removeComment);
router.post('/query-selected-comment',controller.querySelectedComment);

module.exports.router=router;