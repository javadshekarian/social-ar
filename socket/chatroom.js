const {jwtDecode} = require('jwt-decode');

const {chatInsertOne} = require('../usage/chatUsage');

var online = {};
var online_reverse = {};

module.exports.chatroom = io => {
    return io.on('connection',socket=>{

        socket.on('online',data=>{
            const token = jwtDecode(data.token);
            online[token.username] = socket.id;
            online_reverse[socket.id] = token.username;
        })

        socket.on('message',async data=>{
            const token = jwtDecode(data.token);
            const {message,to,from} = data;
            console.log(data);

            await chatInsertOne({message,from,to,reference:token._id});
            
            io.to(online[to]).emit('message',{
                message,
                username:token.username,
                pictureName:token.pictureName
            })
        })
    })
}