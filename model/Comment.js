const mongoose = require('mongoose');
const {User} = require('./User');

const CommentSchema = mongoose.Schema({
    comment:{
        type:String,
        require:true
    },
    isShownInPage:{
        type:Boolean,
        default:false
    },
    reference:{
        type:mongoose.Types.ObjectId,
        ref:User
    }
},{
    timestamps:true
});

const Comment = mongoose.model('Comments',CommentSchema);
module.exports.Comment = Comment;