const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    username:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    pictureName:{
        type:String,
        require:false
    }
},{
    timestamps:true
});

const User = mongoose.model('Users',UserSchema);
module.exports.User = User;