const mongoose = require('mongoose');
const {User} = require('./User');

const ChatSchema = mongoose.Schema({
    message:{
        type:String,
        require:true
    },
    from:{
        type:String,
        require:true
    },
    to:{
        type:String,
        require:true
    },
    reference:{
        type:mongoose.Types.ObjectId,
        ref:User
    }
},{
    timestamps:true
});

const Chat = mongoose.model('Chats',ChatSchema);
module.exports.Chat = Chat;